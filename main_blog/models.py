from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    """A post posted by the user"""
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.text
    
    
class Comment(models.Model):
    """Add a comment"""
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        if len(self.text) < 50:
            return self.text
        else:
            return f"{self.text[:50]}..."