"""Defines URL patterns for main_blog"""

from django.urls import path

from . import views

app_name = 'main_blog'
urlpatterns = [
    # Home page
    path('', views.index, name='index'),

    # Page that shows all posts.
    path('posts/', views.posts, name='posts'),

    # Show a post and their comments.
    path('post/<int:post_id>/', views.post, name='post'),

    # Page for adding a new post.
    path('new_post/', views.new_post, name='new_post'),

    # Page for adding a new comment.
    path('new_comment/<int:post_id>/', views.new_comment, name='new_comment'),

    # Page for editing a comment.
    path('edit_comment/<int:comment_id>/', views.edit_comment, name='edit_comment'),
]