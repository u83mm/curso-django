from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404

from .models import Post, Comment
from .forms import PostForm, CommentForm

def index(request):
    """The home page for main_blog"""

    return render(request, 'main_blog/index.html')

@login_required
def posts(request):
    """Show all posts"""

    posts = Post.objects.filter(owner=request.user).order_by('date_added')

    return render(request, 'main_blog/posts.html', {
        'posts': posts
    })

@login_required
def post(request, post_id):
    """Show a single post and its comments."""
    post = Post.objects.get(id=post_id)
    # Make sure a post belongs to the current user.
    if post.owner != request.user:
        raise Http404
    
    comments = post.comment_set.order_by('-date_added')

    return render(request, 'main_blog/post.html', {
        'post': post,
        'comments': comments
    })

@login_required
def new_post(request):
    """Add a new post."""
    if request.method != 'POST':
        # Create a blank form
        form = PostForm()
    else:
        # Process data.
        form = PostForm(data=request.POST)

        if form.is_valid():
            new_post = form.save(commit=False)
            new_post.owner = request.user
            new_post.save()
            
            return redirect('main_blog:posts')
    
    # Display a blank or invalid form
    return render(request, 'main_blog/new_post.html', {
        'form': form
    })

@login_required
def new_comment(request, post_id):
    """Add a new comment for a particular post."""
    post = Post.objects.get(id=post_id)

    # Make sure a post belongs to the current user.
    if post.owner != request.user:
        raise Http404

    if request.method != 'POST':
        # Create a blank form
        form = CommentForm()
    else:
        # Process data.
        form = CommentForm(data=request.POST)

        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.post = post
            new_comment.save()
            return redirect('main_blog:post', post_id=post_id)
    
    # Display a blank or invalid form
    return render(request, 'main_blog/new_comment.html', {
        'form': form,
        'post': post,
    })

@login_required
def edit_comment(request, comment_id):
    """Edit a comment."""
    comment = Comment.objects.get(id=comment_id)
    post = comment.post

    # Make sure a post belongs to the current user.
    if post.owner != request.user:
        raise Http404

    if request.method != 'POST':
        # Pre-fill form with current comment.
        form = CommentForm(instance=comment)
    else:
        # Process data.
        form = CommentForm(instance=comment, data=request.POST)

        if form.is_valid():
            form.save()
            return redirect('main_blog:post', post_id=post.id)
    
    # Display a blank or invalid form
    return render(request, 'main_blog/edit_comment.html', {
        'form': form,
        'post': post,
        'comment': comment,
    })